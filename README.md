# ToDo App - built with FastAPI

A very simple ToDo app, built with FastAPI.

## Initial Project Setup

```shell
# *ix shell only
git clone git@gitlab.com:augensalat/todo-fastapi.git
cd todo-fastapi
make install

# optionally
. .venv/bin/activate
pip install --upgrade pip
```

## Run the development server

```shell
make server
```
